# Login Authentication System in Flask

## Setup & Installtion

Works best with Python 3.10.6

```bash
git clone https://gitlab.com/super.suloev/kti_flask_auth_postgres.git
```

```bash
pip install -r requirements.txt
```

## Running The App

Flask
```bash
python app.py
```

gunicorn
```bash
gunicorn --bind 0.0.0.0:5000 wsgi:app
```
